from rest_framework import viewsets
from .models import PDU, Machine, Event
from .serializers import PDUSerializer, MachineSerializer, EventSerializer
from .forms import MachineFilter, EventFilter


class PDUViewSet(viewsets.ModelViewSet):
    queryset = PDU.objects.all()
    serializer_class = PDUSerializer


class MachineViewSet(viewsets.ModelViewSet):
    queryset = Machine.objects.all()
    serializer_class = MachineSerializer
    lookup_field = 'mac_address'
    filter_class = MachineFilter


class EventViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    filter_class = EventFilter
    ordering_fields = ('date',)
    ordering = '-date'
