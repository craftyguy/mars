# Generated by Django 3.1.4 on 2021-03-22 15:32

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('infra', '0007_auto_20210316_1545'),
    ]

    operations = [
        migrations.AddField(
            model_name='machine',
            name='pdu_off_delay',
            field=models.PositiveSmallIntegerField(default=5, help_text='Number of seconds to wait in a ON -> OFF -> ON transition', validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(1)]),
        ),
    ]
