from django.apps import AppConfig


class InfraConfig(AppConfig):
    name = 'infra'

    def ready(self):
        import infra.signals  # noqa
