from django.contrib import admin
from .models import PDU, Machine, Event


admin.site.register(PDU)
admin.site.register(Machine)
admin.site.register(Event)
