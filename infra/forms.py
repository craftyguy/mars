from django_filters import rest_framework as filters

from .models import Machine, Event


class MachineFilter(filters.FilterSet):
    class Meta:
        model = Machine
        fields = ('ready_for_service', )


class EventFilter(filters.FilterSet):
    since = filters.DateTimeFilter(field_name='date', lookup_expr='gt')

    class Meta:
        model = Event
        fields = ('since', )
