from deepdiff import DeepDiff
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from infra.models import Machine
from infra.models import Event

import logging
logger = logging.getLogger('mars')


def machine_diff(m1, m2):
    exclusions = [
        # Django-specific field
        '_state',
        # Auto generated, not to be depended on!
        'full_name',
        # Not interesting changes
        'last_updated', 'first_seen', 'id',
    ]

    exclude_paths = []
    for excl in exclusions:
        exclude_paths.append('root.' + excl)
    return DeepDiff(m1, m2, exclude_paths=exclude_paths)


def dispatch_events_for_diff(diff, machine):
    # You can not add a foreign key relation to something that isn't
    # saved, the error that comes when you do is very weird, so fail
    # now!
    assert machine.pk is not None

    serialized_diff = diff.to_json()
    changes = diff.get('values_changed', {})

    if 'root.is_retired' in changes:
        before, after = changes['root.is_retired']['old_value'], \
            changes['root.is_retired']['new_value']
        if not before and after:
            logger.debug(f"{machine} has been retired, generating a retirement event...")
            return Event.objects.create(
                category=Event.CATEGORY_MACHINE_RETIRED,
                machine=machine,
                diff=serialized_diff)
    logger.debug(f"{machine} has been updated")
    return Event.objects.create(
        category=Event.CATEGORY_MACHINE_UPDATED,
        machine=machine,
        diff=serialized_diff)


@receiver(pre_save, sender=Machine)
def machine_pre_save_callback(sender, instance, **kwargs):
    new = instance
    try:
        old = sender.objects.get(pk=new.pk)
    except sender.DoesNotExist:
        instance.__mars_diff__ = '{}'
    else:
        diff = machine_diff(old, new)
        instance.__mars_diff__ = diff


@receiver(post_save, sender=Machine)
def machine_post_save_callback(sender, instance, created, **kwargs):
    assert hasattr(instance, '__mars_diff__')
    diff = instance.__mars_diff__
    if created:
        logger.debug(f"{instance} has been created")
        return Event.objects.create(
            category=Event.CATEGORY_MACHINE_CREATED,
            machine=instance,
            diff=diff)
    else:
        dispatch_events_for_diff(diff, instance)
